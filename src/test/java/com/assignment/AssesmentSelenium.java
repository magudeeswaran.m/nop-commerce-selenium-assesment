package com.assignment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class AssesmentSelenium {
	static WebDriver driver;
	  @BeforeClass
	  public void beforeClass() {
	      
	       driver = new ChromeDriver();
	      driver.get("https://admin-demo.nopcommerce.com/login");
	      driver.manage().window().maximize();
	      driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(15));
	      
	  }
	  
	  @Test
	  public void login() 
	  {
	     WebElement email =  driver.findElement(By.id("Email"));
	     email.clear();
	     email.sendKeys("admin@yourstore.com");
	     WebElement pass = driver.findElement(By.id("Password"));
	     pass.clear();
	     pass.sendKeys("admin");
	      driver.findElement(By.xpath("//button[text()='Log in']")).click();
	      WebElement ele1 = driver.findElement(By.xpath("//a[text()='John Smith']"));
	      String expected = ele1.getText();
	      Assert.assertEquals("John Smith",expected);
	  }
	  
	  @Test(dependsOnMethods = "login")
	  public void Categories() throws IOException, InterruptedException {
	      
	      driver.findElement(By.xpath("//p[contains(text(),'Catalog')]")).click();
	      driver.findElement(By.xpath("//p[contains(text(),'Categories')]")).click();
	      driver.findElement(By.xpath("//a[@class=\"btn btn-primary\"]")).click();
	      
	      File file1 = new File("/home/magudeeswaran/Documents/NopCommerce/TestData1.xlsx");
	        FileInputStream fis1 = new FileInputStream(file1);
	        XSSFWorkbook wbook1 = new XSSFWorkbook(fis1);
	        XSSFSheet st = wbook1.getSheet("Add Category");
	        
	        int rows = st.getLastRowNum();
	        
	        for (int i = 1; i <= rows; i++) {
	            String Name = st.getRow(i).getCell(0).getStringCellValue();
	            String Description = st.getRow(i).getCell(1).getStringCellValue();
	             /*double Price1 = st.getRow(i).getCell(2).getNumericCellValue();
	             String PriceFrom = String.valueOf(Price1);
	             
	            double Price2 = st.getRow(i).getCell(3).getNumericCellValue();
	            String PriceTo = String.valueOf(Price2);*/
	            
	            driver.findElement(By.id("Name")).sendKeys(Name);
	            driver.switchTo().frame("Description_ifr");
	            driver.findElement(By.xpath("//br[@data-mce-bogus=\"1\"]")).sendKeys(Description);
	           driver.switchTo().defaultContent();
	            
	            /*WebElement down = driver.findElement(By.xpath("(//label[@class=\"col-form-label\"])[13]"));
	            
	            JavascriptExecutor js = (JavascriptExecutor)driver;
	            js.executeScript("arguments[0].scrollIntoView(true)",down);*/
	           
	            WebElement parentCatagory = driver.findElement(By.id("ParentCategoryId"));
	        	Select sel = new Select(parentCatagory);
	        	sel.selectByVisibleText("Computers >> Desktops");
	        	
                WebElement down = driver.findElement(By.xpath("(//label[@class=\"col-form-label\"])[13]"));
	            
	            JavascriptExecutor js = (JavascriptExecutor)driver;
	            js.executeScript("arguments[0].scrollIntoView(true)",down);
	        	
	        	double Price1 = st.getRow(i).getCell(2).getNumericCellValue();
	             String PriceFrom = String.valueOf(Price1);
	             driver.findElement(By.xpath("(//input[@class='k-formatted-value k-input'])[2]")).sendKeys(PriceFrom);
	             
	            double Price2 = st.getRow(i).getCell(3).getNumericCellValue();
	            String PriceTo = String.valueOf(Price2);
	            driver.findElement(By.xpath("(//input[@class='k-formatted-value k-input'])[3]")).sendKeys(PriceTo);
	            
	            double displayOrder = st.getRow(i).getCell(4).getNumericCellValue();
	            
	            String display_order = String.valueOf((int)displayOrder);
	            driver.findElement(By.xpath("(//input[@class='k-formatted-value k-input'])[4]")).sendKeys(display_order);
	            
	            Thread.sleep(3000);
	        	
	            driver.findElement(By.xpath("//button[@name='save']")).click();
	            
	            WebElement text1 = driver.findElement(By.xpath("(//td[text()='Computers >> Desktops >> Build your own computer'])[1]"));
	            String expected = text1.getText();
	  	      Assert.assertEquals("Computers >> Desktops >> Build your own computer",expected);
	        }
}
	  @Test(dependsOnMethods = "Categories")
	  public void Products() throws IOException {
		driver.findElement(By.xpath("//p[text()=' Products']")).click();
		
		File file1 = new File("/home/magudeeswaran/Documents/NopCommerce/TestData1.xlsx");
        FileInputStream fis1 = new FileInputStream(file1);
        XSSFWorkbook wbook1 = new XSSFWorkbook(fis1);
        XSSFSheet st = wbook1.getSheet("Add Manufacturer");
        
        int rows = st.getLastRowNum();
        
        for (int i = 1; i <= rows; i++) {
            String productName = st.getRow(i).getCell(0).getStringCellValue();
            driver.findElement(By.id("SearchProductName")).sendKeys(productName);
            WebElement catalog = driver.findElement(By.id("SearchCategoryId"));
            Select sel = new Select(catalog);
        	sel.selectByVisibleText("Computers >> Desktops");
        	driver.findElement(By.id("search-products")).click();
        	WebElement checkProduct = driver.findElement(By.xpath("//td[text()='Build your own computer']"));
        	String expected = checkProduct.getText();
	  	      Assert.assertEquals("Build your own computer",expected);
        }
	}
	  @Test(dependsOnMethods = "Products")
	  public void Manufacturers() throws IOException, InterruptedException {
		  driver.findElement(By.xpath("//p[text()=' Manufacturers']")).click();
		  driver.findElement(By.xpath("//a[@class='btn btn-primary']")).click();
		  
		
		File file1 = new File("/home/magudeeswaran/Documents/NopCommerce/TestData1.xlsx");
        FileInputStream fis1 = new FileInputStream(file1);
        XSSFWorkbook wbook1 = new XSSFWorkbook(fis1);
        XSSFSheet st = wbook1.getSheet("Add Manufacturer");
        
        int rows = st.getLastRowNum();
        
        for (int i = 1; i <= rows; i++) {
        	String Name = st.getRow(i).getCell(1).getStringCellValue();
            String Description = st.getRow(i).getCell(2).getStringCellValue();
        	
        	driver.findElement(By.id("Name")).sendKeys(Name);
        	driver.switchTo().frame("Description_ifr");
            driver.findElement(By.xpath("//br[@data-mce-bogus=\"1\"]")).sendKeys(Description);
           driver.switchTo().defaultContent();
           /* String manufacturerName = st.getRow(i).getCell(0).getStringCellValue();
            driver.findElement(By.id("SearchManufacturerName")).sendKeys(manufacturerName);
            WebElement published = driver.findElement(By.id("SearchPublishedId"));
            Select sel = new Select(published);
        	sel.selectByVisibleText("Published only");*/
           
           
       	
           
       	
       	double Price1 = st.getRow(i).getCell(3).getNumericCellValue();
            String PriceFrom = String.valueOf(Price1);
            driver.findElement(By.xpath("(//input[@class='k-formatted-value k-input'])[2]")).sendKeys(PriceFrom);
            
           double Price2 = st.getRow(i).getCell(4).getNumericCellValue();
           String PriceTo = String.valueOf(Price2);
           driver.findElement(By.xpath("(//input[@class='k-formatted-value k-input'])[3]")).sendKeys(PriceTo);
           
           double displayOrder = st.getRow(i).getCell(5).getNumericCellValue();
           
           String display_order = String.valueOf((int)displayOrder);
           driver.findElement(By.xpath("(//input[@class='k-formatted-value k-input'])[4]")).sendKeys(display_order);
           
           Thread.sleep(3000);
       	
           driver.findElement(By.xpath("//button[@name='save']")).click();
            String manufacturerName = st.getRow(i).getCell(1).getStringCellValue();
           driver.findElement(By.id("SearchManufacturerName")).sendKeys(manufacturerName);
           WebElement published = driver.findElement(By.id("SearchPublishedId"));
           Select sel = new Select(published);
       	sel.selectByVisibleText("Published only");
       	driver.findElement(By.id("search-manufacturers")).click();
       	WebElement checkManufac = driver.findElement(By.xpath("(//td[text()='MagudeesHP'])[1]"));
       	String expected = checkManufac.getText();
	      Assert.assertEquals("MagudeesHP",expected);
		  
        }
	}
	  @Test(dependsOnMethods = "Manufacturers")
	  public void Logout() {
		
		  driver.findElement(By.xpath("//a[text()='Logout']")).click();

	}
	  @AfterClass
	  public void closeApplication() {
		driver.close();

	}
	  }
